Rails.application.routes.draw do
   
  mount SamlCamel::Engine, at: '/saml'
  
  get "administration/index"
  post "administration/expire_emails"
  
#  concern :active_scaffold_association, ActiveScaffold::Routing::Association.new
  concern :active_scaffold, ActiveScaffold::Routing::Basic.new(association: true)
  resources :posix_users, concerns: :active_scaffold

  resources :activity_logs, concerns: :active_scaffold
  resources :users, concerns: :active_scaffold
  resources :admins, concerns: :active_scaffold
  resources :dockers, concerns: :active_scaffold

  # the controller to let users manage their posix attributes
  get "posix/index"
  post "posix/update"

  get "group/index" 
   
  # report on how many containers are in use, free, or powered_down
  get "container_utilization" => "container_utilization#index"
  
  # report on hourly activity
  get "hourly_activity" => "hourly_activity#index"
  

  get 'logo'=>'logo#index'
  get 'information'=>'information#index'
  get 'incommon_login/*path'=>'information#incommon_login'
  get 'information/user_wayf/*path'=>'information#user_wayf'

  # WAYF hack for InCommon
  get 'davidson_login/*path' => 'information#davidson_login'
  get 'duke_login/*path' => 'information#duke_login'
  get 'elon_login/*path' => 'information#elon_login'
  get 'nccu_login/*path' => 'information#nccu_login'

  # bilateral federation with UNC Willmington
  get 'uncw_login/*path' => 'information#uncw_login'
 
  get 'home/index'
  get 'home/login'
  get 'home/logged_in_index', as: 'logged_in_index'  
  get 'home/search'
  get 'home/noaccess', as: :no_access
  get 'home/logout'
  
  root to: 'information#index'
   
  
  get "container_provision" =>"container_provision#index"
  get "container_provision" => "container_provision#index"
  post "/container_provision/expire_all"  
  post "/container_provision/delete_expired"  
  post "/container_provision/expire_emails" 

  resources :containers
  get "containers" =>"containers#index"
  get "containers/:id/show_panel" =>"containers#show_panel"
  get '/containers/renew', :to => 'containers#renew',  :as =>"containers_renew"  
      
  match '/containers/rstudio', :to => 'containers#rstudio', :via => :get, :as =>"containers_rstudio"  
  match '/containers/nccu-rstudio', :to => 'containers#nccu-rstudio', :via => :get, :as =>"containers_nccu-rstudio"  
  match '/containers/davidson-csc-110-0', :to => 'containers#davidson-csc-110-0', :via => :get, :as =>"containers_davidson-csc-110-0"  
  match '/containers/davidson-pol-182-a', :to => 'containers#davidson-pol-182-a', :via => :get, :as =>"containers_davidson-pol-182-a"  
  match '/containers/jupyter/*redirect', :to => 'containers#jupyter', :via => :get, :as =>"containers_jupyter"  
  match '/containers/pyroot/*redirect', :to => 'containers#pyroot', :via => :get, :as =>"containers_pyroot"  
  match '/containers/nccu-jupyter/*redirect', :to => 'containers#nccu-jupyter', :via => :get, :as =>"containers_nccu-jupyter"  
  match '/containers/davidson-mat-315-0/*redirect', :to => 'containers#davidson-mat-315-0', :via => :get, :as =>"containers_davidson-mat-315-0"  
  match '/containers/pytorch', :to => 'containers#pytorch', :via => :get, :as =>"containers_pytorch"  
  match '/containers/nccu-pytorch', :to => 'containers#nccu-pytorch', :via => :get, :as =>"containers_nccu-pytorch"  
  match '/containers/davidson-vscode', :to => 'containers#davidson-vscode', :via => :get, :as =>"containers_davidson-vscode"  
  match '/containers/knime', :to => 'containers#knime', :via => :get, :as =>"containers_knime"  
 
     
  match '/containers/restart_rstudio', :to => 'containers#restart', :via => :get, :as =>"containers_restart_rstudio"  
  match '/containers/restart_nccu-rstudio', :to => 'containers#restart', :via => :get, :as =>"containers_restart_nccu-rstudio"  
  match '/containers/restart_davidson-csc-110-0', :to => 'containers#restart', :via => :get, :as =>"containers_restart_davidson-csc-110-0"  
  match '/containers/restart_davidson-pol-182-a', :to => 'containers#restart', :via => :get, :as =>"containers_restart_davidson-pol-182-a"  
  match '/containers/restart_jupyter', :to => 'containers#restart', :via => :get, :as =>"containers_restart_jupyter"
  match '/containers/restart_pyroot', :to => 'containers#restart', :via => :get, :as =>"containers_restart_pyroot"
  match '/containers/restart_nccu-jupyter', :to => 'containers#restart', :via => :get, :as =>"containers_restart_nccu-jupyter"
  match '/containers/restart_davidson-mat-315-0', :to => 'containers#restart', :via => :get, :as =>"containers_restart_davidson-mat-315-0"
  match '/containers/restart_pytorch', :to => 'containers#restart', :via => :get, :as =>"containers_restart_pytorch"
  match '/containers/restart_nccu-pytorch', :to => 'containers#restart', :via => :get, :as =>"containers_restart_nccu-pytorch"
  match '/containers/restart_davidson-vscode', :to => 'containers#restart', :via => :get, :as =>"containers_restart_davidson-vscode"
  match '/containers/restart_knime', :to => 'containers#restart', :via => :get, :as =>"containers_restart_knime"
     
 
  match '/containers/requestrestart_rstudio', :to => 'containers#requestrestart', :via => :get, :as =>"containers_requestrestart_rstudio"  
  match '/containers/requestrestart_nccu-rstudio', :to => 'containers#requestrestart', :via => :get, :as =>"containers_requestrestart_nccu-rstudio"  
  match '/containers/requestrestart_davidson-csc-110-0', :to => 'containers#requestrestart', :via => :get, :as =>"containers_requestrestart_davidson-csc-110-0"  
  match '/containers/requestrestart_davidson-pol-182-a', :to => 'containers#requestrestart', :via => :get, :as =>"containers_requestrestart_davidson-pol-182-a"  
  match '/containers/requestrestart_jupyter', :to => 'containers#requestrestart', :via => :get, :as =>"containers_requestrestart_jupyter"  
  match '/containers/requestrestart_pyroot', :to => 'containers#requestrestart', :via => :get, :as =>"containers_requestrestart_pyroot"  
  match '/containers/requestrestart_nccu-jupyter', :to => 'containers#requestrestart', :via => :get, :as =>"containers_requestrestart_nccu-jupyter"  
  match '/containers/requestrestart_davidson-mat-315-0', :to => 'containers#requestrestart', :via => :get, :as =>"containers_requestrestart_davidson-mat-315-0"  
  match '/containers/requestrestart_pytorch', :to => 'containers#requestrestart', :via => :get, :as =>"containers_requestrestart_pytorch"  
  match '/containers/requestrestart_nccu-pytorch', :to => 'containers#requestrestart', :via => :get, :as =>"containers_requestrestart_nccu-pytorch"  
  match '/containers/requestrestart_davidson-vscode', :to => 'containers#requestrestart', :via => :get, :as =>"containers_requestrestart_davidson-vscode"  
  match '/containers/requestrestart_knime', :to => 'containers#requestrestart', :via => :get, :as =>"containers_requestrestart_knime"  
  
    
end
