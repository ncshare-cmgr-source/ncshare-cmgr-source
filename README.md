# NCShare.org container manager application source

This project contains the source code for the NCShare.org container manager reservations system. Container manager (cmgr) is a part of the Compute-as-a-Service portion of the [ncshare.org](ncshare.org) initiative - a partnership between Duke University, Davidson College, North Carolina Central University and MCNC.

Cmgr is used to manage reservations for Docker containers used for coursework. Students and instructors visit the cmgr.ncshare.org site,  select their authentication method / identity provider (IDP) and are then able to reserve and access course containers for applications such as JupyterLab, RStudio, etc. 

Cmgr supports both authentication by InCommon IDPs for partner institutions, and also supports bilateral authentication via Microsoft Azure Active Directory SAML (a.k.a. EntraID).

This work is sponsored by the NSF under award #2201105.

## How to build and deploy

The container manager build/deploy process automated with a gitlab continuous integration script which calls a couple bash shell scripts to create a Docker container for the application. An example of the .gitlab-ci.yml script is found in the file 'example-gitlab-ci.yml'. So that we can quickly rollback to previous versions of the application, each build is tagged with a timestamp and the directory tree that held the source used for the build is tagged and retained. Look at the build-production and build-test files for details.

To run the application we use docker-compose to orchestrate launch of a mariadb (mysql) database container, an nginx web proxy container, and the container manager Ruby-on-Rails application. See the file docker-compose-production for details. Example settings for the nginx web proxy are found in the nginx directory.


## License

Copyright 2023-present Duke University

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


## Technical notes


### server configuration for cmgr

To keep the database and some config files independent of the source code
found in the git repository, there are a couple of assumptions (and symlinks)

The directory /srv/persistent-data/ncshare-datadirs is assumed to be present 
in the deployed instance and it holds some key files:

1.) .env -> /srv/persistent-data/ncshare-datadirs/env

2.) mysql -> /srv/persistent-data/ncshare-datadirs/cmgr-vmm/mysql 

This is where we keep the mariadb/mysql container's database files 
In addition to the symlinks, at runtime, docker-compose is configured 
to mount several volumes containing sensitive/persistent info:

     - /srv/persistent-data/ncshare-datadirs/cmgr-vmm/secrets.yml:/app/config/secrets.yml
     - /srv/persistent-data/ncshare-datadirs/cmgr-vmm/azure_rsa:/app/config/azure_rsa
     - /srv/persistent-data/ncshare-datadirs/cmgr-vmm/shared/:/app/shared:
     - /srv/persistent-data/ncshare-datadirs/cmgr-vmm/saml:/app/config/saml

*secrets.yml* is the Ruby-on-Rails secrets file

*azure_rsa* the the private key used to start remote ssh sessions to launch or restart  
containers (typically in azure or in this case ncshare.org)

*shared* is the shared directory for Rails (not currently used)

*saml* is where the SAML/shibboleth settings are held

Inside the saml directory, there is a file named
````
          incommon_idp_metadata
````
where we store the metadata for the IDPs that we allow users to authenticate against.
This is a subset of the commplete InCommon IDP metadata file - both to speed up searches 
and limit access to NCShare member institutions. When we add more schools, this file will
need to be updated.

### authenticated deep links

So that instructors can publish links to specific containers in course syllabii we 
need a way to deep link to a specifric container while also authenticating the user. 
The deep link URLs should start with 
````
          https://cmgr.ncshare.org/incommon_login
````
and the rest of the URL should specify the container type. For example, to send the
user to the jupyter container the deep link URL is:
````
          https://cmgr.ncshare.org/incommon_login/containers/jupyter
````
If the user does not yet have a reservation, the deep link will first send the user through the
InCommon WAYF site to pick an IDP, then authentdcate the suer, then reserve a container instance.

If a reservation exists, the user will be authenticated and then dropped into the container.

