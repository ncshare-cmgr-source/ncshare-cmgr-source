class DropMiscTables < ActiveRecord::Migration[6.0]
  def up
    drop_table :dockersJan3
    drop_table :dockersJul17
    drop_table :dockersMay11
    drop_table :dockersjan4
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
