class CreateVmRequests < ActiveRecord::Migration
  def change
    create_table :vm_requests do |t|
      t.references :user
      t.string :technical_contact_netid
      t.string :technical_contact_name
      t.text :other_contacts
      t.string :proj_name
      t.boolean :tandc
      t.string :image_name
      t.text :info
      t.string :hostname
      t.text :description
      t.string :shortname
      t.string :ip
      t.integer :step, :default => 0
      t.timestamps
    end
  end
end
