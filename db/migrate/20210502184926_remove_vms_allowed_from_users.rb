class RemoveVmsAllowedFromUsers < ActiveRecord::Migration[6.0]
  def change
    remove_column :users, :vms_allowed, :integer
  end
end
