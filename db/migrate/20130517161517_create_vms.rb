class CreateVms < ActiveRecord::Migration
  def change
    create_table :vms do |t|
      t.integer :network_resource_id
      t.integer :image_id
      t.string :admin_password

      t.timestamps
    end
  end
end
