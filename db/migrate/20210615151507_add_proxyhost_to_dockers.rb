class AddProxyhostToDockers < ActiveRecord::Migration[6.0]
  def change
    add_column :dockers, :proxyhost, :string
  end
end
