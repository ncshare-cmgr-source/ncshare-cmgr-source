class CreateVehicles < ActiveRecord::Migration[6.0]
  def change
    create_table :vehicles do |t|
      t.integer :m_id
      t.string :chassis
      t.string :type
      t.string :yr_made
      t.string :yr_ended
      t.string :doors
      t.string :drive_train
      t.string :e_mnt
      t.integer :hp
      t.integer :trq
      t.integer :e_id
      t.string :trans

      t.timestamps
    end
  end
end
