class CreateJobSlots < ActiveRecord::Migration
  def change
    create_table :job_slots do |t|
      t.string :job_type
      t.integer :delayed_job_id
      t.boolean :active, :default => true
      t.timestamps
    end
    
    add_index :job_slots, :job_type
    add_index :job_slots, :delayed_job_id
  end
end
