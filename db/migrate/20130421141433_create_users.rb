class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :duid
      t.string :netid
      t.string :displayName
      t.string :phone
      t.string :email
      t.integer :vms_allowed, :default=>1

      t.timestamps
    end
  end
end
