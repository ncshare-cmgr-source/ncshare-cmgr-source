class AlterReservations < ActiveRecord::Migration
  def up
    remove_column :reservations, :start_date
    remove_column :reservations, :end_date
    add_column :reservations, :renewed, :boolean, default: true
  end

  def down
    add_column :reservations, :start_date, :date
    add_column :reservations, :end_date, :date
    remove_column :reservations, :renewed
  end
end
