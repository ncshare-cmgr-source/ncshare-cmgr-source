class CreateJoinTableGroupImage < ActiveRecord::Migration
  def change
    create_table :groups_images, :id => false do |t|
      t.integer :group_id
      t.integer :image_id
    end

    add_index :groups_images, :group_id
    add_index :groups_images, :image_id
    
    # Add the group we were actually using up to this point to all current images
    ActiveRecord::Base.connection.execute("INSERT INTO groups_images (group_id, image_id)" +
     "SELECT 1, id FROM images")      
    
  end
end
