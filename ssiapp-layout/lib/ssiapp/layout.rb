require "ssiapp/layout/version"

module Ssiapp
  module Layout
    class Engine < ::Rails::Engine
      isolate_namespace Ssiapp
      config.to_prepare do
        ApplicationController.helper(FormHelper)
        ApplicationController.helper(ApplicationHelper)
      end

      initializer :assets do |app|
        app.config.assets.precompile += %w(ssiapp/*)
      end
    end
    # Your code goes here...
  end
end
