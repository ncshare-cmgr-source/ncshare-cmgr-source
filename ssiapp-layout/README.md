# Ssiapp::Layout

The standard layout for SSI Automation apps



## Usage

Add this line to your application_controler.rb:

layout 'ssiapp/application'

That'll get you using the layout.  In addition there are a few helper methods
you can define that'll let you customize the layout somewhat.  This is how you
add navigation links.

- title_str - the title element for the html page (name of the tab for the
              webpage) - uses Rails.application.class.parent_name by default
- application_name - String to put in header bar of each page - uses
                     Rails.application.class.parent_name by default
- nav_links - Return Array of links to put in primary navbar.  Each member
              should be a Hash with the keys :class and :link
- nav_right - HTML to be included in a div on the right of the navbar
- secondary_nav_links - Return Array of links to put in the secondary navbar.
                        Each element should be the link itself
- logo - path to Logo image.  Defaults to: ssiapp/logo.png

## Caveats

Make sure your application.js includes jquery.   Bootstrap requires it.  The
application application.js is loaded before the javascript for the layout so
that the app can load jquery for us.
