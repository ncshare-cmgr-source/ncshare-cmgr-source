class UsersController < ApplicationController
  layout 'ssiapp/application'
  require 'will_paginate'
  
  before_action :login_required
  before_action :set_user  #If they have logged in
  before_action :onlyAdmin

  active_scaffold :"user" do |conf|
    #conf.actions = [ :show, :list, :search, :nested]
    conf.list.per_page = 25
    conf.columns[:activity_logs].form_ui = :select
  end



end


