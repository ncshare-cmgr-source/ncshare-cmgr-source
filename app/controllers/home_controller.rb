class HomeController < ApplicationController
  
  before_action :login_required
  before_action :set_user  #If they have logged in

  def index
    return if @current_user.blank?

    @alert = []

    #@current_user.reservations.where(status: 'expired').each do |r|
    #  @alert << "You are #{@current_user}"
    #end
  end
  
  def logged_in_index
    render :index
  end
  
  def login 
    login_and_continue(logged_in_index_url)
  end
  
  def logout
    # kill the SP session, reset the user to nil then
    # send them to the front page since we cannot depend on IDPs having a logout URL
    SamlCamel::Logging.logout(session[:saml_attributes])
    session[:saml_attributes] = nil
    session[:sp_session] = nil
    @current_user = nil
    reset_session 
    redirect_to "https://cmgr.ncshare.org/"
  end
end

