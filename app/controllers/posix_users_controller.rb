class PosixUsersController < ApplicationController
  layout 'ssiapp/application'
  before_action :login_required
  before_action :set_user  #If they have logged in
  before_action :onlyAdmin

  require 'will_paginate'

  before_action :login_required
  before_action :set_user  #If they have logged in
  before_action :onlyAdmin

  active_scaffold :"posix_user" do |conf|
    #conf.actions = [ :show, :list, :search, :nested]
    conf.list.per_page = 25
    columns[:user].weight = 0
    columns[:posix_active].weight = 1
    columns[:institution].weight = 2
    columns[:gitlab_user].weight = 3
    columns[:gitlab_displayname].weight = 4
    columns[:gitlab_public_key].weight = 7
    columns[:shell_preference].weight = 5
    columns[:ldap_entry].weight = 6
    columns[:updated_at].weight = 8
    conf.list.sorting = { :id => :asc }
    conf.list.per_page = 50
    config.columns = [ :user,  :posix_active, :institution, :gitlab_user, :gitlab_displayname, :gitlab_public_key, :shell_preference, :ldap_entry, :updated_at]
    list.columns.exclude :created_at

    #conf.columns[:users].form_ui = :select
  end

end
