class HourlyActivityController < ApplicationController
  layout 'ssiapp/application'
  before_action :login_required
  before_action :set_user  #If they have logged in
  before_action :onlyAdmin
  
  
  def check_hourly_activity
    # get hourly counts of activity_log events - a good proxy for users redirecting to containers
    previous_day = []
    24.downto(1) do |i|
      start = i.to_s
      stop = (i-1).to_s
      sql_query = "select start,end,activity from (select now() - INTERVAL #{start} HOUR as start) as a, (select now() - INTERVAL #{stop} HOUR as end) as b,
      (select count(*) as activity from activity_logs WHERE occurred_at >= now() - INTERVAL #{start} HOUR and occurred_at <= now() - INTERVAL #{stop} HOUR) as c"
      results = ActiveRecord::Base.connection.exec_query(sql_query)
      previous_day.push(results.first)
    end  
    return previous_day
  end
  
  def check_hourly_activity_60_days
    # get 60 days of hourly counts of activity_log events - a good proxy for users redirecting to containers
    previous_day = []
    1440.downto(1) do |i|
      start = i.to_s
      stop = (i-1).to_s
      sql_query = "select start,end,activity from (select curdate() - INTERVAL #{start} HOUR as start) as a, (select curdate() - INTERVAL #{stop} HOUR as end) as b,
      (select count(*) as activity from activity_logs WHERE occurred_at >= curdate() - INTERVAL #{start} HOUR and occurred_at <= curdate() - INTERVAL #{stop} HOUR) as c"
      results = ActiveRecord::Base.connection.exec_query(sql_query)
      previous_day.push(results.first)
    end  
    # require 'json'
    #   File.open("tmp/temp.json","w") do |f|
    # f.write(previous_day.to_json)
    return previous_day
  end
  
  def index
    @hourly_activity = self.check_hourly_activity
  end

end
