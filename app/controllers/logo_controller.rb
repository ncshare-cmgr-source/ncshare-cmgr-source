class LogoController < ApplicationController
  

  layout 'ssiapp/application'
  #  before_action :login_required
  #  before_action :set_user  #If they have logged in
  #  before_action :onlyAdmin
    skip_before_action :saml_protect
    skip_before_action :set_user
  
  # GET /logo
  def index
    render :file => 'public/ncshare-cmgr-logo.png', :layout => false 
  end


end
