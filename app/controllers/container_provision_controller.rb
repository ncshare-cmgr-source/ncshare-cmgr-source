class ContainerProvisionController < ApplicationController
  layout 'ssiapp/application'
  before_action :login_required
  before_action :set_user  #If they have logged in
  before_action :onlyAdmin
  
  def index
    sql = "select * from dockers order by id;"
    sql = "select dr.appname, dr.netid, dr.expired, dr.pw, dr.host, dr.port, dr.updated_at, " +
      " u.displayName from dockers dr left outer join users u on dr.netid = u.netid " +
      " order by dr.id"
    @containers = Docker.find_by_sql(sql)
  end

  def expire_all
    sql = "select * from dockers where netid != '';"
    @containers_in_use = Docker.find_by_sql(sql)
    @containers_in_use.each do |item|
      item.expired = true
      item.save!
    end
    flash[:notice] = "All container reservations have been marked as expired"
    redirect_to action: :index
  end 
  
  def delete_expired
    sql = "select * from dockers where netid != '' and expired is TRUE;"
    @stale_containers = Docker.find_by_sql(sql)
    @stale_containers.each do |item|
      ActivityLog.log(session[:current_user_id], 'Container Provision',"Container flushed: #{item.netid}\t#{item.appname}\t#{item.host}\t#{item.port}")      
      item.netid = ''
      item.expired = false
      item.save!
    end   
    flash[:notice] = "All expired container reservations have been deleted"
    redirect_to action: :index
  end
  
  def expire_emails
    sql = "select distinct netid from dockers where expired is TRUE;"
    @expired_container_netids = Docker.find_by_sql(sql)
  end
   

end
