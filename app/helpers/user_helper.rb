module UserHelper
	def format(key,sa)
		val = sa.send(key)
		if key.eql? "created_at" 
			val  = val.localtime.strftime("%Y-%m-%d %H:%M") unless val.blank?
		end
		return val
	end
  
end
