class CorgiController < ApplicationController
  protect_from_forgery with: :exception
  before_action except: [:index] do
    saml_protect(relay_state: "potato")
  end

  def index
  end

  def show
  end

  def use_saml
    saml_protect(relay_state: "potato")
  end
end
